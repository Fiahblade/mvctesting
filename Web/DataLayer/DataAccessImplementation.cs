﻿using System;
using System.Collections.Generic;
using Web.Models;

namespace Web.DataLayer
{
    public class DataAccessImplementation
    {
        private List<User> users;

        public DataAccessImplementation()
        {
            this.users = new List<User> { new User("Robin Gabriël", new DateTime(1994, 10, 8)), new User("Bernard Delbeke", new DateTime(1997, 4, 9)), new User("Toon Doesselaere", new DateTime(1997, 4, 4)) };
        }

        public List<User> Users { get => users; set => users = value; }
    }
}