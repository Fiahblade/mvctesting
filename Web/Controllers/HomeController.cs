﻿using System.Web.Mvc;
using Web.DataLayer;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            DataAccessImplementation data = new DataAccessImplementation();

            return View(data.Users);
        }
    }
}