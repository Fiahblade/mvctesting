﻿using System.Linq;
using System.Web.Mvc;
using Web.DataLayer;
using Web.Models;

namespace Web.Controllers
{
    public class PlayerController : Controller
    {
        public ActionResult Index(string name)
        {
            DataAccessImplementation data = new DataAccessImplementation();
            User user = data.Users.First(p => p.Name == name);

            return View(user);
        }
    }
}