﻿using System;

namespace Web.Models
{
    public class User
    {
        private string name;
        private DateTime birthday;

        public User(string name, DateTime birthday)
        {
            this.name = name;
            this.birthday = birthday;
        }

        public string Name { get => name; set => name = value; }

        public DateTime Birthday { get => birthday; set => birthday = value; }
    }
}